@servers(['web'=>'u971687650@194.163.47.108 -p 65002'])

@setup
  $path = '/home/u971687650/domains/homologacao.net.br/public_html/deploy-app';
@endsetup

@story('deploy')
  git
  composer
  config
  route
@endstory

@task('git')
  cd {{ $path }}
  {{-- git reset --hard origin/main --}}
  git pull origin main
@endtask

@task('composer')
  cd {{ $path }}
  composer install
@endtask

@task('config')
  cd {{ $path }}
  php -r "file_exists('.env') || copy('.env.example', '.env');"
  php artisan package:discover --ansi
  php artisan vendor:publish --tag=laravel-assets --ansi --force
  php artisan key:generate --ansi
  php artisan config:clear
  php artisan config:cache
@endtask

@task('route')
  cd {{ $path }}
  php artisan route:clear
  php artisan route:cache
@endtask

